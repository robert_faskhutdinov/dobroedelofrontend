<?php

defined('_JEXEC') or die;

function modChrome_dobromenu($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
		<div class="dobro-back submenu">
			<div class="moduletable<?php echo htmlspecialchars($params->get('moduleclass_sfx')); ?>">
				<div class="submenu-title">
					<?php if ((bool) $module->showtitle) : ?>
						<p><span class="arrowdown glyphicon glyphicon-chevron-down"></span>
							<?php echo $module->title; ?>
						</p>
					<?php endif; ?>
				</div>
				<div class="submenu-body">
					<?php echo $module->content; ?>
				</div>
			</div>
		</div>
	<?php endif;
} ?>

<?php function modChrome_dobroright($module, &$params, &$attribs)
{
	if (!empty ($module->content)) : ?>
		<div class="dobro-back">
			<?php if ((bool) $module->showtitle) : ?>
				<h3>
					<?php echo $module->title; ?>
				</h3>
			<?php endif; ?>
			<div class="dobro-news">
				<?php echo $module->content; ?>
			</div>
		</div>
	<?php endif;
} ?>