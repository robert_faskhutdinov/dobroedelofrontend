<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$item_heading = $params->get('item_heading', 'h4');
$images = json_decode($item->images);
?>
<div class="dobro-one-news">
	<?php  if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
		<a href="<?php echo $item->link;?>">
			<img class="img-thumbnail" src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
		</a>
	<?php endif; ?>

	<?php $publish_date = strtotime($item->publish_up); ?>

	<p><?php echo date('d.m.Y' ,$publish_date); ?></p>

	<?php if ($params->get('item_title')) : ?>

		<<?php echo $item_heading; ?> class="<?php echo $params->get('moduleclass_sfx'); ?>">
		<?php if ($params->get('link_titles') && $item->link != '') : ?>
			<a href="<?php echo $item->link;?>">
				<?php echo $item->title;?></a>
		<?php else : ?>
			<?php echo $item->title; ?>
		<?php endif; ?>
		</<?php echo $item_heading; ?>>

	<?php endif; ?>

	<p>
		<?php echo $item->introtext; ?>
	</p>

	<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) :
		echo '<a class="readmore" href="'.$item->link.'">'.$item->linkText.'</a>';
	endif; ?>
</div>