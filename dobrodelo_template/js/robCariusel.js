var rob = rob || {};

rob.carousel = {
	items: {},
	mainWidth: 0,
	viewportWidth: 0,
	elementWidth: 0,
	init: function() {
		var that = this;
		that.isAnimate = false;
		that.items = $('.crsl-item');
		that.viewportWidth = $('.rob-carousel-body').width();
		that.elementWidth = $(that.items[0]).outerWidth();
		that.mainWidth = that.elementWidth * that.items.length;

		that.leftArrow = $('.rob-carousel-arrow-left');
		that.rightArrow = $('.rob-carousel-arrow-right');
		that.crslItemsEl = $('.crsl-items');

		that.rightArrow.on('click', function(){
			var leftStr = that.crslItemsEl.css('left');
			var left = +leftStr.substring(0,leftStr.length-2);
			var diff = that.mainWidth - that.viewportWidth + left;
			if (diff > 0 && !that.isAnimate) {
				that.isAnimate = true;
				that.crslItemsEl.animate({'left' : left - that.elementWidth +'px'}, 500, 'swing', function(){
					that.isAnimate = false;
				});
			}
		});

		that.leftArrow.on('click', function(){
			var leftStr = that.crslItemsEl.css('left');
			var left = +leftStr.substring(0,leftStr.length-2);
			if (left < 0 && !that.isAnimate) {
				that.isAnimate = true;
				that.crslItemsEl.animate({'left' : left + that.elementWidth +'px'}, 500, 'swing', function(){
				that.isAnimate = false;
			});
			}
		});
	}
}