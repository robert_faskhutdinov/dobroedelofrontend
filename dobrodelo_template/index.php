<?php
	defined('_JEXEC') or die;
?>

<!DOCTYPE html>
<html>
<head>
	<jdoc:include type="head"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="templates/system/css/system.css" type="text/css" />
	<link rel="stylesheet" href="templates/dobrodelo/bower_components/bootstrap/dist/css/bootstrap.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="templates/dobrodelo/css/dobroedelo.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:700|Merriweather:400,900|Playfair+Display:400,900,700italic|Oswald:700|PT+Mono"
		  type="text/css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400normal&subset=latin,cyrillic">
	<link rel="icon" type="image/png" href="templates/dobrodelo/images/favicon.png" />
</head>
<body class="dobro-birds">


<div class="dobro-header">
	<div class="container">
		<div class="dobroheader-left">
			<a href="http://www.dobro-delo.ru">
				<img class="dobro-logo" src="templates/dobrodelo/images/dobro-logo5.png">
			</a>
		</div>

		<div class="dobroheader-right">

			<div class="social-links text-right">
				<a href="http://vk.com/b.f.dobroedelo_izhevsk" target="_blank"><img src="templates/dobrodelo/images/ico/vk.png"></a>
				<a href="http://my.mail.ru/community/b.f.dobroedelo_izhevsk" target="_blank"><img src="templates/dobrodelo/images/ico/ma.png"></a>
				<a href="https://www.facebook.com/groups/b.f.dobroedelo" target="_blank"><img src="templates/dobrodelo/images/ico/fb.png"></a>
				<a href="https://twitter.com/bf_dobroedelo" target="_blank"><img src="templates/dobrodelo/images/ico/tw.png"></a>
				<a href="http://odnoklassniki.ru/group/52008363819155" target="_blank"><img src="templates/dobrodelo/images/ico/ok.png"></a>
			</div>

			<div class="input-group dobro-search align-right">
				<jdoc:include type="modules" name="search" style="xhtml" />
			</div>

			<div class="text-right">
				<jdoc:include type="modules" name="donate_button" style="xhtml" />
			</div>

		</div>

	</div>
</div>

<div class="dobro-menu">
	<div class="container">
		<jdoc:include type="modules" name="mainmenu" style="xhtml" />
	</div>
</div>

<div class="">
	<div class="container dobro-content">
		<div class="rob-carousel">
			<jdoc:include type="modules" name="slider" style="xhtml" />
		</div>

		<div class="main-column dobro-back">
			<div class="breadcrumbs">
				<jdoc:include type="modules" name="breadcrumbs" style="xhtml" />
			</div>
			<jdoc:include type="component" />
		</div>

		<div class="right-column">

			<jdoc:include type="modules" name="side_menu1" style="dobromenu" />


			<jdoc:include type="modules" name="news" style="dobroright" />

		</div>

	</div>
</div>
<div class="dobro-footer">
	<jdoc:include type="modules" name="footer" style="xhtml" />
	<div class="container relative">
		<div class="footer-heart">
			<img style="height: 80px;" src="templates/dobrodelo/images/footer-heart.png"/>
		</div>
		<p class="text-center">&copy; Благотворительный фонд &laquo;Доброе дело&raquo;</p>
	</div>
</div>

<!--Place for scripts-->
<script type="text/javascript" src="templates/dobrodelo/bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript" src="templates/dobrodelo/bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript" src="templates/dobrodelo/js/robCariusel.js"></script>

<script type="text/javascript">
	rob.carousel.init();
</script>

</body>
</html>